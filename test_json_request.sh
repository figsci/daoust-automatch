if [ $1 = 'STOP' ]
then
 curl localhost:1334/automatch?stop
 exit 0
fi
if [ $1 = 'NTHREAD' ]
then
  curl localhost:1334/automatch?nthread
  exit 0
fi
if [ $1 = 'LOG' ]
then
 curl localhost:1334/automatch?log=$2
 exit 0
fi
body=`cat data/test_$1.json`
curl --data-ascii "$body" localhost:1334/automatch
