import unidecode
import textract
import PyPDF2
from PIL import Image
import pytesseract
import cv2
import subprocess
import os
import wand.image

def readfile(fullpath):
    filename = fullpath.rsplit('/',1)[1]
    if filename.endswith('.docx'):
        text = read_docx(fullpath)
        return text
    if filename.endswith('.txt'):
        text = read_text(fullpath)
        return text
    if filename.endswith('.pdf'):
        text = read_pdf(fullpath)
        return text
    if filename.endswith('.doc'):
        text = read_doc(fullpath)
        return text
    if filename.endswith('.PNG') or filename.endswith('.png') or filename.endswith('.JPG') or filename.endswith('.jpg') or filename.endswith('.jpeg'):
        preprocessed_image = preprocess_image(fullpath)
        text = read_image(preprocessed_image)
        return text    

def read_doc(fullpath):
    directory = fullpath.rsplit('/',1)[0]
    filename = fullpath.rsplit('/',1)[1]
    soficeloc = r"C:\Program Files\LibreOffice\program\soffice.exe"
    print('calling subprocess')
    subprocess.call([soficeloc, '--headless', '-convert-to', 'docx', fullpath, '--outdir', directory])
    new_filename = filename.replace('.doc', '.docx')
    new_fullpath = '{}/{}'.format(directory, new_filename)
    text = read_docx(new_fullpath)
    os.remove(new_fullpath)
    return text

def read_docx(fullpath):
    filename = fullpath.rsplit('/',1)[1]
    try:
        return unidecode.unidecode(textract.process(fullpath).decode("utf-8"))
    except UnicodeDecodeError:
        return unidecode.unidecode(textract.process(fullpath).decode("latin-1"))
    except:
        print('Error reading docx')

def read_text(fullpath):
    filename = fullpath.rsplit('/',1)[1]
    try:
        return unidecode.unidecode(open(fullpath, encoding='utf-8').read())
    except UnicodeDecodeError:
        return unidecode.unidecode(open(fullpath, encoding='latin-1').read())    
    except:
        print('Error reading text file')

def read_pdf(fullpath):
    text = ''
    file = open(fullpath, 'rb')
    fileReader = PyPDF2.PdfFileReader(file)
    for p in range(fileReader.numPages):
        pageObj = fileReader.getPage(p)
        text += pageObj.extractText()
    clean_text = text.replace('\n','')
    if len(clean_text) >= 5:
        return unidecode.unidecode(text) 
    else:
        text = pdf_to_image_to_text(fullpath)
        return text

def pdf_to_image_to_text(fullpath):
    # Take apart PDF into individual image for each page
    directory = fullpath.rsplit('/',1)[0]
    text = ''
    with wand.image.Image(filename=fullpath, resolution=500) as pdf:
        for i, page in enumerate(pdf.sequence):
            with wand.image.Image(page) as img:
                filename = "{}/{}_{}.png".format(directory, os.getpid(), i+1)
                img.format= 'png'
                img.alpha_channel = False # Set to false to keep white background
                img.save(filename= filename)
                preprocessed_image = preprocess_image(filename)
                os.remove(filename)
                text += read_image(preprocessed_image)
    return text
                

def preprocess_image(fullpath):
    directory = fullpath.rsplit('/',1)[0]
    image = cv2.imread(fullpath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    filename = "{}/{}.png".format(directory,os.getpid())
    cv2.imwrite(filename, gray)
    return filename

def read_image(fullpath):
    pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
    text = pytesseract.image_to_string(Image.open(fullpath), )
    os.remove(fullpath)
    return text