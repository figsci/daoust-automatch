
# coding: utf-8

# # Load libraries

# In[1]:


import pandas as pd
import textract
import PyPDF2
import re
import unidecode
from datetime import datetime
import os,sys
#import time
import numpy as np
#import itertools
import subprocess
#import os
import glob
import langdetect

#import argparse
import io
from google.cloud import vision
from google.cloud.vision import types

#from sys import getsizeof
#import gc

import nltk #import the natural language toolkit library
from nltk.stem.snowball import FrenchStemmer #import the French stemming library
from nltk.stem.snowball import DutchStemmer #import the Dutch stemming library
from nltk.corpus import stopwords #import stopwords from nltk corpus
import re #import the regular expressions library; will be used to strip punctuation
#from collections import Counter #allows for counting the number of occurences in a list

from wand.image import Image
from wand.color import Color

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:80% !important; }</style>"))


# # Load essential data and functions

# In[2]:


#Load data
FrenchDict = pd.read_csv('../Data/input/liste_francais.txt', encoding='latin-1')
FirstNames = pd.read_csv('../Data/input/Prenoms.csv', delimiter=';',encoding='latin-1')
Softwares = pd.read_csv('../Data/middleput/SoftwareCleaned.csv')
JobtitlesDaoust = pd.read_csv('../Data/middleput/JobTitlesDaoustv4.csv',encoding='utf-8')
#JobtitlesDaoust.set_index(JobtitlesDaoust['JobTitle'], inplace=True)
#KBOCompany = pd.read_csv('../Data/middleput/KBOCompany.csv')
Company = pd.read_csv('../Data/middleput/CompaniesCleaned.csv')
Driverlicense = pd.read_excel('../Data/input/Permis.xlsx')
Brevet = pd.read_csv('../Data/middleput/BrevetCleaned.csv')


# In[3]:


#Define functions
def detect_text_uri(path):
    client = vision.ImageAnnotatorClient()
    
    if path.startswith('http') or path.startswith('gs:'):
        image = types.Image()
        image.source.image_uri = path

    else:
        with io.open(path, 'rb') as image_file:
            print(path)
            content = image_file.read()

        image = types.Image(content=content)


    response = client.text_detection(image=image)
    texts = response.text_annotations
    return texts[0].description

def ReadFile(path, filename):
    if filename[-5:] == '.docx':
        try:
            return unidecode.unidecode(textract.process(path + filename).decode("utf-8"))
        except:
            print('Reading in latin-1: {}'.format(filename))
            return unidecode.unidecode(textract.process(path + filename).decode("latin-1"))
    if filename[-4:] == '.txt':
        try:
            return unidecode.unidecode(open(path + filename, encoding='utf-8').read())
        except:
            print('Reading in latin-1: {}'.format(filename))
            return unidecode.unidecode(open(path + filename, encoding='latin-1').read())
    if filename[-4:] == '.pdf':
        text = ''
        file = open(path + filename, 'rb')
        fileReader = PyPDF2.PdfFileReader(file)
        for p in range(fileReader.numPages):
            pageObj = fileReader.getPage(p)
            text += pageObj.extractText()
        return unidecode.unidecode(text)  


def CleanDataTier1(data): #basic cleaning
    if type(data) == str:
        data = unidecode.unidecode(data)
        data = data.replace('(','').replace(')','').replace('*','').replace('+','').replace('[','').replace(']','').replace('|','').replace('∆','').replace('?','') #remove
        data = data.replace('\t',' ').replace('\n',' ').replace(':',' ').replace('-',' ').replace(';',' ').replace('\\',' ').replace('.',' ').replace(',',' ').replace('\'',' ').replace('/',' ').replace('<',' ')  #replace by spaces
        output = data.lower() #to lower case
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            element = unidecode.unidecode(element)
            element = element.replace('(','').replace(')','').replace('*','').replace('+','').replace('[','').replace(']','').replace('|','').replace('∆','').replace('?','') #remove
            element = element.replace('\t',' ').replace('\n',' ').replace(':',' ').replace('-',' ').replace(';',' ').replace('\\',' ').replace('.',' ').replace(',',' ').replace('\'',' ').replace('/',' ').replace('<',' ') #replace by spaces
            element = element.lower() #to lower case
            output.append(element)
        return output
    
def CleanDataTier2(data, stopword_df, language, min_len=1, delete_list=[]): #removing unnecessary words
    stopword_list = stopword_df[stopword_df['Language']==language]['Stopword_list'].values[0]
    if type(data) == str:
        words = data.split() #split text into words
        filtered_words = [] #declare an empty list to hold our filtered words
        for word in words: #iterate over all words from the text
            if word not in stopword_list and len(word) > min_len: #only add words that are not in the French stopwords list, and are more than 1 character
                filtered_words.append(word) #add word to filter_words list if it meets the above conditions
        output = ' '.join(filtered_words)
        
        for d in delete_list:
            output = output.replace(d, '')
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            words= element.split() #split text into words
            filtered_words = [] #declare an empty list to hold our filtered words
            for word in words: #iterate over all words from the text
                if word not in stopword_list and len(word) > min_len: #only add words that are not in the French stopwords list, and are more than 1 character
                    filtered_words.append(word) #add word to filter_words list if it meets the above conditions
            data = ' '.join(filtered_words)
            for d in delete_list:
                data.replace(d, '')
            output.append(data)
        return output
    

def CleanDataTier3(data, language): #stem words
    if language == "FR":
        stemmer = FrenchStemmer()
    if language == "NL":
        stemmer = DutchStemmer()
      
    if type(data) == str:
        words = data.split() #split text into words
        stemmed_words = [] #declare an empty list to hold our stemmed words
        for word in words:
            stemmed_word=stemmer.stem(stemmer.stem(stemmer.stem(word))) #stem the word three times to be sure
            stemmed_words.append(stemmed_word) #add it to our stemmed word list
        output = ' '.join(stemmed_words)
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            words = element.split() #split text into words
            stemmed_words = [] #declare an empty list to hold our stemmed words
            for word in words:
                stemmed_word=stemmer.stem(stemmer.stem(stemmer.stem(word))) #stem the word three times to be sure
                stemmed_words.append(stemmed_word) #add it to our stemmed word list
            data = ' '.join(stemmed_words)   
            output.append(data)
        return output

        
def search_term(term, text):
    #print(term)
    a = re.search(r'\b'+str(term)+' ', text)
    try:
        return a.start() / len(text)
    except:
        return None
    
def search_term_all(term, text):
    #print(term)
    a = re.search(str(term), text)
    try:
        return a.start() / len(text)
    except:
        return None
    
def check_exist(term, text_set):
    return term in text_set
    
    
def search_email(text):
    match = re.search(r'[\w\.-]+@[\w\.-]+', text)
    try:
        return match.group(0)
    except:
        return None
    
def search_driver(text):
    match_list =['permis b', 'permis de conduire b', 'permis conduire b']
    a = re.search(r'\b'+str(match_list)+' ', text)
    try:
        a.start()
        return 'Permis B'
    except:
        return None    
    
#French word checker
french_words = set(word.strip() for word in CleanDataTier1(FrenchDict.iloc[:,0].astype(str)))
def is_french_word(word):
    return word in french_words

#Name checker
first_names = set(word.strip() for word in CleanDataTier1(FirstNames.iloc[:,0].astype(str)))
def is_first_name(word):
    return word in first_names

#Birth date serach
def search_birth(text):
    a = re.search(r'((0[1-9]|[1-3]\d)(0[1-9]|1[012])[12][09]\d{2})', text) #regex on different types of birthdates
    try: 
        a = a.group()
        return a[:2] +'/' + a[2:4]+ '/' + a[4:]
    except:
        return None       
    
#Phone search
def search_phone(text):
    a = re.search(r'(04\d{8}|4\d{7}|01\d{7}|0032\d{7,9})', text) #regex on differnt types of phone numbers
    try: 
        return a.group()
    except:
        return None

def transform_toset(df, column, combination_length):
    output = []
    splitted = df[column].str.split()
    for index, row in df.iterrows():
        l = len(row.text1)
        for i in range(2,combination_length):
            base = splitted[index]
            inter = []
            for j in range(l-i):
                joined = ' '.join(base[j:j+i])
                inter += [joined] 
            base += inter
        output.append(set(base))    
    return output

def detect_language(text):
    try: 
        res = langdetect.detect_langs(text)
        for item in res:
            if item.lang == "fr" or item.lang == "nl":
                return item.lang.upper()
        return None
    except:
        return 'Problem with text'
#Define constants
raw_stopword_list_fr = ["Ap.", "Apr.", "GHz", "MHz", "USD", "a", "afin", "ah", "ai", "aie", "aient", "aies", "ait", "alors", "après", "as", "attendu", "au", "au-delà", "au-devant", "aucun", "aucune", "audit", "auprès", "auquel", "aura", "aurai", "auraient", "aurais", "aurait", "auras", "aurez", "auriez", "aurions", "aurons", "auront", "aussi", "autour", "autre", "autres", "autrui", "aux", "auxdites", "auxdits", "auxquelles", "auxquels", "avaient", "avais", "avait", "avant", "avec", "avez", "aviez", "avions", "avons", "ayant", "ayez", "ayons", "b", "bah", "banco", "ben", "bien", "bé", "c", "c'", "c'est", "c'était", "car", "ce", "ceci", "cela", "celle", "celle-ci", "celle-là", "celles", "celles-ci", "celles-là", "celui", "celui-ci", "celui-là", "celà", "cent", "cents", "cependant", "certain", "certaine", "certaines", "certains", "ces", "cet", "cette", "ceux", "ceux-ci", "ceux-là", "cf.", "cg", "cgr", "chacun", "chacune", "chaque", "chez", "ci", "cinq", "cinquante", "cinquante-cinq", "cinquante-deux", "cinquante-et-un", "cinquante-huit", "cinquante-neuf", "cinquante-quatre", "cinquante-sept", "cinquante-six", "cinquante-trois", "cl", "cm", "cm²", "comme", "contre", "d", "d'", "d'après", "d'un", "d'une", "dans", "de", "depuis", "derrière", "des", "desdites", "desdits", "desquelles", "desquels", "deux", "devant", "devers", "dg", "différentes", "différents", "divers", "diverses", "dix", "dix-huit", "dix-neuf", "dix-sept", "dl", "dm", "donc", "dont", "douze", "du", "dudit", "duquel", "durant", "dès", "déjà", "e", "eh", "elle", "elles", "en", "en-dehors", "encore", "enfin", "entre", "envers", "es", "est", "et", "eu", "eue", "eues", "euh", "eurent", "eus", "eusse", "eussent", "eusses", "eussiez", "eussions", "eut", "eux", "eûmes", "eût", "eûtes", "f", "fait", "fi", "flac", "fors", "furent", "fus", "fusse", "fussent", "fusses", "fussiez", "fussions", "fut", "fûmes", "fût", "fûtes", "g", "gr", "h", "ha", "han", "hein", "hem", "heu", "hg", "hl", "hm", "hm³", "holà", "hop", "hormis", "hors", "huit", "hum", "hé", "i", "ici", "il", "ils", "j", "j'", "j'ai", "j'avais", "j'étais", "jamais", "je", "jusqu'", "jusqu'au", "jusqu'aux", "jusqu'à", "jusque", "k", "kg", "km", "km²", "l", "l'", "l'autre", "l'on", "l'un", "l'une", "la", "laquelle", "le", "lequel", "les", "lesquelles", "lesquels", "leur", "leurs", "lez", "lors", "lorsqu'", "lorsque", "lui", "lès", "m", "m'", "ma", "maint", "mainte", "maintes", "maints", "mais", "malgré", "me", "mes", "mg", "mgr", "mil", "mille", "milliards", "millions", "ml", "mm", "mm²", "moi", "moins", "mon", "moyennant", "mt", "m²", "m³", "même", "mêmes", "n", "n'avait", "n'y", "ne", "neuf", "ni", "non", "nonante", "nonobstant", "nos", "notre", "nous", "nul", "nulle", "nº", "néanmoins", "o", "octante", "oh", "on", "ont", "onze", "or", "ou", "outre", "où", "p", "par", "par-delà", "parbleu", "parce", "parmi", "pas", "passé", "pendant", "personne", "peu", "plus", "plus_d'un", "plus_d'une", "plusieurs", "pour", "pourquoi", "pourtant", "pourvu", "près", "puisqu'", "puisque", "q", "qu", "qu'", "qu'elle", "qu'elles", "qu'il", "qu'ils", "qu'on", "quand", "quant", "quarante", "quarante-cinq", "quarante-deux", "quarante-et-un", "quarante-huit", "quarante-neuf", "quarante-quatre", "quarante-sept", "quarante-six", "quarante-trois", "quatorze", "quatre", "quatre-vingt", "quatre-vingt-cinq", "quatre-vingt-deux", "quatre-vingt-dix", "quatre-vingt-dix-huit", "quatre-vingt-dix-neuf", "quatre-vingt-dix-sept", "quatre-vingt-douze", "quatre-vingt-huit", "quatre-vingt-neuf", "quatre-vingt-onze", "quatre-vingt-quatorze", "quatre-vingt-quatre", "quatre-vingt-quinze", "quatre-vingt-seize", "quatre-vingt-sept", "quatre-vingt-six", "quatre-vingt-treize", "quatre-vingt-trois", "quatre-vingt-un", "quatre-vingt-une", "quatre-vingts", "que", "quel", "quelle", "quelles", "quelqu'", "quelqu'un", "quelqu'une", "quelque", "quelques", "quelques-unes", "quelques-uns", "quels", "qui", "quiconque", "quinze", "quoi", "quoiqu'", "quoique", "r", "revoici", "revoilà", "rien", "s", "s'", "sa", "sans", "sauf", "se", "seize", "selon", "sept", "septante", "sera", "serai", "seraient", "serais", "serait", "seras", "serez", "seriez", "serions", "serons", "seront", "ses", "si", "sinon", "six", "soi", "soient", "sois", "soit", "soixante", "soixante-cinq", "soixante-deux", "soixante-dix", "soixante-dix-huit", "soixante-dix-neuf", "soixante-dix-sept", "soixante-douze", "soixante-et-onze", "soixante-et-un", "soixante-et-une", "soixante-huit", "soixante-neuf", "soixante-quatorze", "soixante-quatre", "soixante-quinze", "soixante-seize", "soixante-sept", "soixante-six", "soixante-treize", "soixante-trois", "sommes", "son", "sont", "sous", "soyez", "soyons", "suis", "suite", "sur", "sus", "t", "t'", "ta", "tacatac", "tandis", "te", "tel", "telle", "telles", "tels", "tes", "toi", "ton", "toujours", "tous", "tout", "toute", "toutefois", "toutes", "treize", "trente", "trente-cinq", "trente-deux", "trente-et-un", "trente-huit", "trente-neuf", "trente-quatre", "trente-sept", "trente-six", "trente-trois", "trois", "très", "tu", "u", "un", "une", "unes", "uns", "v", "vers", "via", "vingt", "vingt-cinq", "vingt-deux", "vingt-huit", "vingt-neuf", "vingt-quatre", "vingt-sept", "vingt-six", "vingt-trois", "vis-à-vis", "voici", "voilà", "vos", "votre", "vous", "w", "x", "y", "z", "zéro", "à", "ç'", "ça", "ès", "étaient", "étais", "était", "étant", "étiez", "étions", "été", "étée", "étées", "étés", "êtes", "être", "ô"]
raw_stopword_list_nl = ['a', 'aan', 'aangaande', 'aangezien', 'achter', 'achterna', 'aen', 'af', 'afd', 'afgelopen', 'agter', 'al', 'aldaar', 'aldus', 'alhoewel', 'alias', 'alle', 'allebei', 'alleen', 'alleenlyk', 'allen', 'alles', 'als', 'alsnog', 'altijd', 'altoos', 'altyd', 'ander', 'andere', 'anderen', 'anders', 'anderszins', 'anm', 'b', 'behalve', 'behoudens', 'beide', 'beiden', 'ben', 'beneden', 'bent', 'bepaald', 'beter', 'betere', 'betreffende', 'bij', 'bijna', 'bijvoorbeeld', 'bijv', 'binnen', 'binnenin', 'bizonder', 'bizondere', 'bl', 'blz', 'boven', 'bovenal', 'bovendien', 'bovengenoemd', 'bovenstaand', 'bovenvermeld', 'buiten', 'by', 'daar', 'daarheen', 'daarin', 'daarna', 'daarnet', 'daarom', 'daarop', 'daarvanlangs', 'daer', 'dan', 'dat', 'de', 'deeze', 'den', 'der', 'ders', 'derzelver', 'des', 'deszelfs', 'deszelvs', 'deze', 'dezelfde', 'dezelve', 'dezelven', 'dezen', 'dezer', 'dezulke', 'die', 'dien', 'dikwijls', 'dikwyls', 'dit', 'dl', 'doch', 'doen', 'doet', 'dog', 'door', 'doorgaand', 'doorgaans', 'dr', 'dra', 'ds', 'dus', 'echter', 'ed', 'een', 'eene', 'eenen', 'eener', 'eenig', 'eenige', 'eens', 'eer', 'eerdat', 'eerder', 'eerlang', 'eerst', 'eerste', 'eersten', 'effe', 'egter', 'eigen', 'eigene', 'elk', 'elkanderen', 'elkanderens', 'elke', 'en', 'enig', 'enige', 'enigerlei', 'enigszins', 'enkel', 'enkele', 'enz', 'er', 'erdoor', 'et', 'etc', 'even', 'eveneens', 'evenwel', 'ff', 'gauw', 'ge', 'gebragt', 'gedurende', 'geen', 'geene', 'geenen', 'gegeven', 'gehad', 'geheel', 'geheele', 'gekund', 'geleden', 'gelijk', 'gelyk', 'gemoeten', 'gemogen', 'geven', 'geweest', 'gewoon', 'gewoonweg', 'geworden', 'gezegt', 'gij', 'gt', 'gy', 'haar', 'had', 'hadden', 'hadt', 'haer', 'haere', 'haeren', 'haerer', 'hans', 'hare', 'heb', 'hebben', 'hebt', 'heeft', 'hele', 'hem', 'hen', 'het', 'hier', 'hierbeneden', 'hierboven', 'hierin', 'hij', 'hoe', 'hoewel', 'hun', 'hunne', 'hunner', 'hy', 'ibid', 'idd', 'ieder', 'iemand', 'iet', 'iets', 'ii', 'iig', 'ik', 'ikke', 'ikzelf', 'in', 'indien', 'inmiddels', 'inz', 'inzake', 'is', 'ja', 'je', 'jezelf', 'jij', 'jijzelf', 'jou', 'jouw', 'jouwe', 'juist', 'jullie', 'kan', 'klaar', 'kon', 'konden', 'krachtens', 'kunnen', 'kunt', 'laetste', 'lang', 'later', 'liet', 'liever', 'like', 'm', 'maar', 'maeken', 'maer', 'mag', 'martin', 'me', 'mede', 'meer', 'meesten', 'men', 'menigwerf', 'met', 'mezelf', 'mij', 'mijn', 'mijnent', 'mijner', 'mijzelf', 'min', 'minder', 'misschien', 'mocht', 'mochten', 'moest', 'moesten', 'moet', 'moeten', 'mogelijk', 'mogelyk', 'mogen', 'my', 'myn', 'myne', 'mynen', 'myner', 'myzelf', 'na', 'naar', 'nabij', 'nadat', 'naer', 'net', 'niet', 'niets', 'nimmer', 'nit', 'no', 'noch', 'nog', 'nogal', 'nooit', 'nr', 'nu', 'o', 'of', 'ofschoon', 'om', 'omdat', 'omhoog', 'omlaag', 'omstreeks', 'omtrent', 'omver', 'onder', 'ondertussen', 'ongeveer', 'ons', 'onszelf', 'onze', 'onzen', 'onzer', 'ooit', 'ook', 'oorspr', 'op', 'opdat', 'opnieuw', 'opzij', 'opzy', 'over', 'overeind', 'overigens', 'p', 'pas', 'pp', 'precies', 'pres', 'prof', 'publ', 'reeds', 'rond', 'rondom', 'rug', 's', 'sedert', 'sinds', 'sindsdien', 'sl', 'slechts', 'sommige', 'spoedig', 'st', 'steeds', 'sy', 't', 'tamelijk', 'tamelyk', 'te', 'tegen', 'tegens', 'ten', 'tenzij', 'ter', 'terwijl', 'terwyl', 'thans', 'tijdens', 'toch', 'toe', 'toen', 'toenmaals', 'toenmalig', 'tot', 'totdat', 'tusschen', 'tussen', 'tydens', 'u', 'uit', 'uitg', 'uitgezonderd', 'uw', 'uwe', 'uwen', 'uwer', 'vaak', 'vaakwat', 'vakgr', 'van', 'vanaf', 'vandaan', 'vanuit', 'vanwege', 'veel', 'veeleer', 'veelen', 'verder', 'verre', 'vert', 'vervolgens', 'vgl', 'vol', 'volgens', 'voor', 'vooraf', 'vooral', 'vooralsnog', 'voorbij', 'voorby', 'voordat', 'voordezen', 'voordien', 'voorheen', 'voorop', 'voort', 'voortgez', 'voorts', 'voortz', 'vooruit', 'vrij', 'vroeg', 'vry', 'waar', 'waarom', 'wanneer', 'want', 'waren', 'was', 'wat', 'we', 'weer', 'weg', 'wege', 'wegens', 'weinig', 'weinige', 'wel', 'weldra', 'welk', 'welke', 'welken', 'welker', 'werd', 'werden', 'werdt', 'wezen', 'wie', 'wiens', 'wier', 'wierd', 'wierden', 'wij', 'wijzelf', 'wil', 'wilde', 'worden', 'wordt', 'wy', 'wyze', 'wyzelf', 'zal', 'ze', 'zeer', 'zei', 'zeker', 'zekere', 'zelf', 'zelfde', 'zelfs', 'zelve', 'zelven', 'zelvs', 'zich', 'zichzelf', 'zichzelve', 'zichzelven', 'zie', 'zig', 'zij', 'zijn', 'zijnde', 'zijne', 'zijner', 'zo', "zo'n", 'zoals', 'zodra', 'zommige', 'zommigen', 'zonder', 'zoo', 'zou', 'zoude', 'zouden', 'zoveel', 'zowat', 'zulk', 'zulke', 'zulks', 'zullen', 'zult', 'zy', 'zyn', 'zynde', 'zyne', 'zynen', 'zyner', 'zyns']
delete_words_nl = ['medewerker', 'medewerkster', 'arbeider', 'arbeidster', 'werker', 'werkster', 'bediende']
t1_raw_stopword_dict = {'Stopword_list': [CleanDataTier1(raw_stopword_list_fr), CleanDataTier1(raw_stopword_list_nl)], 'Language': ['FR', 'NL']}
t1_raw_stopwords = pd.DataFrame(t1_raw_stopword_dict)

permis_stopword_list_fr = [word for word in raw_stopword_list_fr if word not in ["b", "be", "c", "ce","e", "d", "g", "am"]]
permis_stopword_list_nl = [word for word in raw_stopword_list_nl if word not in ["b", "be", "c", "ce","e", "d", "g", "am"]]
t1_permis_stopword_dict = {'Stopword_list': [CleanDataTier1(permis_stopword_list_fr), CleanDataTier1(permis_stopword_list_nl)], 'Language': ['FR', 'NL']}
t1_permis_stopwords = pd.DataFrame(t1_permis_stopword_dict)


# # Convert unreadable datatypes

# In[4]:


get_ipython().run_cell_magic('script', 'false', '\n#Convert .doc files to .txt files\nfiledirectory = "C:\\\\Users\\Jerome\\Agilytic\\Agilytic - Documents\\Daoust\\AutoMatch\\Data\\TestDocConvert"\nsoficeloc = "C:\\Program Files\\LibreOffice\\program\\soffice.exe"\n\nfor filename in os.listdir(filedirectory):\n    print(filename)\n    if filename.endswith(\'.doc\'):\n        input_path = filedirectory + \'\\\\\' + filename\n        subprocess.call([soficeloc, \'--headless\', \'-convert-to\', \'text\', input_path, \'--outdir\', filedirectory])')


# In[5]:


get_ipython().run_cell_magic('script', 'false', '#Split pdf into images\n\n#fname = "CV Mustapha.pdf"\nfiledirectory = "C:/Users/Jerome/Agilytic/Agilytic - Documents/Daoust/AutoMatch/Data/TestPdfConvert"\npath = "C:/Users/Jerome/Agilytic/Agilytic - Documents/Daoust/AutoMatch/Data/TestPdfConvert/"\n\n\nfor filename in os.listdir(filedirectory): \n    try:\n        # creating an object \n        file = open(path + filename, \'rb\')\n        # creating a pdf reader object\n        fileReader = PyPDF2.PdfFileReader(file)\n\n        # print the number of pages in pdf file\n        pageObj = fileReader.getPage(0)\n        if len(pageObj.extractText()) < 5: \n\n            # Take apart PDF into individual image for each page\n            page_images = []\n            with Image(filename=path + filename, resolution=500) as pdf:\n                for i, page in enumerate(pdf.sequence):\n                    with Image(page) as img:\n                        print(filename[:-4])\n                        img_name = "{}_{}.png".format(filename[:-4], i+1)\n                        img.format= \'png\'\n                        img.alpha_channel = False # Set to false to keep white background\n                        img.save(filename= path + img_name)\n                        page_images.append(img_name)\n                        #Apply ocr\n                        input_path = path + img_name\n                        print(input_path)\n                        text = detect_text_uri(input_path)\n                        text_file = open(filedirectory + \'\\\\\' + filename[:-4]+ str(i+1) + \'.txt\', "w", encoding=\'utf-8\')\n                        text_file.write(text)\n                        text_file.close()  \n    except:\n        print(\'Error opening file: {}\'.format(path + filename))\'\'\'')


# In[6]:


get_ipython().run_cell_magic('script', 'false', 'files = []\nfor file in glob.glob("C:/Users/Jerome/Agilytic/Agilytic - Documents/Daoust/AutoMatch/Data/TestPdfConvert/*.txt"):\n    files.append(file[:-5])\nunique_files = list(set(files))\nunique_files')


# In[7]:


get_ipython().run_cell_magic('script', 'false', '#Concatenate text files \nfiledirectory = "C:/Users/Jerome/Agilytic/Agilytic - Documents/Daoust/AutoMatch/Data/TestPdfConvert"\noutputdirectory = "C:/Users/Jerome/Agilytic/Agilytic - Documents/Daoust/AutoMatch/Data/TrainingSet/"\n\nfor file in unique_files:\n    print(file[83:])\n    with open(outputdirectory+ file[83:] +\'.txt\', \'w\', encoding=\'utf-8\') as outfile:\n        for part in glob.glob(file +\'*.txt\'):\n            with open(part, encoding=\'utf-8\') as infile:\n                outfile.write(infile.read())\n    outfile.close()')


# In[8]:


get_ipython().run_cell_magic('script', 'false', '#apply Google vision ocr to images\n\nfiledirectory = "C:\\\\Users\\Jerome\\Agilytic\\Agilytic - Documents\\Daoust\\AutoMatch\\Data\\TestImageConvert"\nfiles_in_directory = os.listdir(filedirectory)\nprint(files_in_directory)\n\n#Only one file\nfiles_in_directory = [\'IMG_20181211_205215.jpg\', \'IMG_20181211_205133.jpg\']\n#print(files_in_directory)\n\nfor filename in files_in_directory:\n    if filename.endswith(\'.PNG\') or filename.endswith(\'.png\') or filename.endswith(\'.JPG\') or filename.endswith(\'.jpg\'):\n        print(filename)\n        input_path = filedirectory + \'\\\\\' + filename\n        text = detect_text_uri(input_path)\n        text_file = open(filedirectory + \'\\\\\' + filename[:-4] + \'.txt\', "w", encoding=\'utf-8\')\n        text_file.write(text)\n        text_file.close()   \n    if filename.endswith(\'.jpeg\'):\n        print(filename)\n        input_path = filedirectory + \'\\\\\' + filename\n        text = detect_text_uri(input_path)\n        text_file = open(filedirectory + \'\\\\\' + filename[:-5] + \'.txt\', "w", encoding=\'utf-8\')\n        text_file.write(text)\n        text_file.close()')


# In[9]:


get_ipython().run_cell_magic('script', 'false', "#Extract text from pdf\n# creating an object \nfile = open('../Data/Sample CV/CV_DeBoeverManu180115.pdf', 'rb')\n\n# creating a pdf reader object\nfileReader = PyPDF2.PdfFileReader(file)\n\n# print the number of pages in pdf file\nprint(fileReader.numPages)\ntext = ''\nfor p in range(fileReader.numPages):\n    pageObj = fileReader.getPage(p)\n    text += pageObj.extractText()\nprint(text)")


# # Prepare lookup lists

# In[10]:


#Prepare jobtitle data

#Common base
JobtitlesDaoust['Cleaned1'] = JobtitlesDaoust.apply(lambda row: CleanDataTier1(row['Term']), axis=1)
JobtitlesDaoust['Cleaned2'] = JobtitlesDaoust.apply(lambda row: CleanDataTier2(row['Cleaned1'], t1_raw_stopwords, row['Language'], delete_list=delete_words_nl), axis=1)
JobtitlesDaoust['Cleaned3'] = JobtitlesDaoust.apply(lambda row: CleanDataTier3(row['Cleaned1'], row['Language']), axis=1)
JobtitlesDaoust['Cleaned3spaceless'] = JobtitlesDaoust['Cleaned3'].str.replace(' ', '')

#Intermediate
Jobtitle_intermediate = JobtitlesDaoust
Jobtitle_restrictive = JobtitlesDaoust
remove_list_fr = ['profess', 'programm', 'ger', 'enseign', 'instit', 'magasin', 'pont', 'cadr', 'port','voitur','telephon','cour', 'cours']
remove_list_nl = ['las']
remove_list = remove_list_fr + remove_list_nl

Jobtitle_intermediate = Jobtitle_intermediate[~Jobtitle_intermediate['Cleaned3'].isin(remove_list)]


# In[11]:


#Prepare company data

#Common base
Company['Cleaned1'] = Company.apply(lambda row: CleanDataTier1(row['Term']), axis=1)
#Company['Cleaned2'] = Company.apply(lambda row: CleanDataTier2(row['Cleaned1'], CleanDataTier1(raw_stopword_list), 0), axis=1)
#Company['Cleaned3'] = Company.apply(lambda row: CleanDataTier3(CleanDataTier3(row['Term'])), axis=1)

#Intermediate
Company_intermediate = Company
remove_list1 = ['Maison', 'CHU','Formations', 'Mars', 'LA', 'AU', 'ams', 'LT', 'du', 'c', 'ACCESS', 'VTT', 'HR+', 'De', 'L\'Europe', 'ANS', 'Études', 'Lycée', 'ActiVite','Bel']
remove_list2 = ['curriculum vitae','ar','engels','option','ac','ge','microsoft','esprit','interim','art','di','mise en place','di','marie','eigen','joseph','pac','commercial',
                'plus','cia','etc','paul','page','unive','universit','ces','di','club','sap','monsieur','ab','arts','b2','bru','cesi','excell','petit','jan','europe','idi',
                'action','verder','str','jeunesse','profil','ceb','clinique','gsma','max','midi','michel','le monde','boulanger','industrie','avant','la boulangerie','ses',
                'wallonie','cpas','open','hel','bruxelles formation','expert','agence','dialogue','syntra','bd','ucl','mission','iss','face','ipes','dialog','express']

Company_intermediate = Company[~Company['Term'].isin(remove_list1)]
Company_intermediate = Company_intermediate[~Company_intermediate['Cleaned1'].isin(remove_list2)]
Company_intermediate = Company_intermediate[Company_intermediate['Term'].str.len()>1]


# In[12]:


#Prepare software data

#Common base
Softwares['Cleaned1'] = Softwares.apply(lambda row: CleanDataTier1(row['Term']), axis=1)
Softwares = Softwares[Softwares['Cleaned1'].str.replace(' ','').str.len()>1] #longer than 1 character
#Company['Cleaned2'] = Company.apply(lambda row: CleanDataTier2(row['Cleaned1'], CleanDataTier1(raw_stopword_list)), axis=1)
#Company['Cleaned3'] = Company.apply(lambda row: CleanDataTier3(CleanDataTier3(row['Term'])), axis=1)

#Intermediate
Softwares_intermediate = Softwares
remove_list = ['Stock', 'Chef', 'Gmail', 'Microsoft Office', 'Internet Explorer', 'Vue','surface','access','windows','outlook com','project','publisher','store','fusion',
                'adobe','vi','comp','facebook','go','security','encore','poser','maya']

remove_list2 = ['Winbooks']
Softwares_intermediate = Softwares_intermediate[~Softwares_intermediate['Cleaned1'].isin(CleanDataTier1(remove_list))]
Softwares_intermediate = Softwares_intermediate[~Softwares_intermediate['Software'].isin(remove_list2)]


# In[13]:


#Prepare brevet data

#Common base
Brevet['Cleaned1'] = Brevet.apply(lambda row: CleanDataTier1(row['Term']), axis=1)

#Intermediate
Brevet_intermediate = Brevet


# # Prepare lookup functions

# In[14]:


#Define functions that will look for lists of terms

def list_search1(term_df, term_cleaningtype, doc_cleaningtype, output_name, doc_row):
    term_df = term_df.loc[:,[term_cleaningtype, output_name]]
    term_df['position'] = term_df.apply(lambda term_row: search_term(term_row[term_cleaningtype], doc_row[doc_cleaningtype]), axis=1) #Get list of all the terms that match
    term_df = term_df.dropna(subset=['position'])
    term_df = term_df.sort_values(by='position', ascending=True) #Sort output by first occurence and only terms that matched
    output = term_df.loc[:,[output_name]].drop_duplicates(keep='first').values #Output the unique values
    return output

def list_search1_set(term_df, term_cleaningtype, doc_cleaningtype, output_name, doc_row):
    term_df = term_df.loc[:,[term_cleaningtype, output_name]] #keep only relevant columns
    term_df['exists'] = term_df.apply(lambda term_row: check_exist(term_row[term_cleaningtype], doc_row[doc_cleaningtype + '_set']), axis=1) #Get list of all the terms that exist
    #print(doc_row[doc_cleaningtype + '_set'])
    #term_df['exists'] = check_exist(term_df[term_cleaningtype].values, doc_row[doc_cleaningtype + '_set'])
    term_df = term_df[term_df['exists']]
    try:
        term_df['position'] = term_df.apply(lambda term_row: search_term(term_row[term_cleaningtype], doc_row[doc_cleaningtype]), axis=1) #Get list of all the terms that match
        term_df = term_df.sort_values(by='position', ascending=True) #Sort output by first occurence and only terms that matched
        output = term_df.loc[:,[output_name]].drop_duplicates(keep='first').values #Output the unique values
        return output
    except:
        return []

def list_search2(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,doc2_cleaningtype, output_name, doc_row):
    if doc_row['language'] == 'FR':
        search_function = search_term
        term1_df = term1_df[term1_df['Language']=='FR']
        term2_df = term2_df[term2_df['Language']=='FR']
    if doc_row['language'] == 'NL':
        search_function = search_term_all
        term1_df = term1_df[term1_df['Language']=='NL']
        term2_df = term2_df[term2_df['Language']=='NL']
    
    term1_df = term1_df.loc[:,[term1_cleaningtype, output_name]]
    term2_df = term2_df.loc[:,[term2_cleaningtype, output_name]]
    term1_df['position'] = term1_df.apply(lambda term_row: search_function(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype]), axis=1) #Get list of all the terms that match
    term2_df['position'] = term2_df.apply(lambda term_row: search_function(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype]), axis=1) #Get list of all the terms that match
    term_df = term1_df.append(term2_df, sort=True).dropna(subset=['position'])
    term_df = term_df.sort_values(by='position', ascending=True) #Sort output by first occurence and only terms that matched
    output = term_df.loc[:,[output_name]].dropna().drop_duplicates(keep='first').values #Output the unique values
    return output

def list_search2_set(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,doc2_cleaningtype, output_name,doc_row):
    if doc_row['language'] == 'FR':
        search_function = search_term
        term1_df = term1_df[term1_df['Language']=='FR']
        term2_df = term2_df[term2_df['Language']=='FR']
    if doc_row['language'] == 'NL':
        search_function = search_term_all
        term1_df = term1_df[term1_df['Language']=='NL']
        term2_df = term2_df[term2_df['Language']=='NL']
        
    term1_df = term1_df.loc[:,[term1_cleaningtype, output_name]]
    term1_df['exists'] = term1_df.apply(lambda term_row: check_exist(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype + '_set']), axis=1) #Get list of all the terms that exist
    term1_df = term1_df[term1_df['exists']] #Keep only the ones found in set
    try: 
        term1_df['position'] = term1_df.apply(lambda term_row: search_function(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype]), axis=1) #Get list of all the terms that match
    except:
        pass
        
    term2_df = term2_df.loc[:,[term2_cleaningtype, output_name]]
    term2_df['exists'] = term2_df.apply(lambda term_row: check_exist(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype + '_set']), axis=1) #Get list of all the terms that exist
    term2_df = term2_df[term2_df['exists']] #Keep only the ones found in set
    try:
        term2_df['position'] = term2_df.apply(lambda term_row: search_function(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype]), axis=1) #Get list of all the terms that match
    except:
        pass
    
    try:
        term_df = term1_df.append(term2_df, sort=True).dropna(subset=['position'])
        term_df = term_df.sort_values(by='position', ascending=True) #Sort output by first occurence and only terms that matched
        output = term_df.loc[:,[output_name]].dropna().drop_duplicates(keep='first').values #Output the unique values
        return output
    except:
        return []

def jobtitle_search(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,doc2_cleaningtype, output_name,doc_row):
    if doc_row['language'] == 'FR':
        term2_cleaningtype = 'Cleaned3'
        doc2_cleaningtype = 'text3'
        output = list_search2_set(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,doc2_cleaningtype, output_name,doc_row)        
        
    if doc_row['language'] == 'NL':
        term2_cleaningtype = 'Cleaned3'
        doc2_cleaningtype = 'text3'
        output = list_search2(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,doc2_cleaningtype, output_name,doc_row)
        
    return output
    


# In[15]:


#Validate on testset

#Path of cv's
path = "../Data/TrainingSet/"
 
if len(sys.argv) == 2:
    path = sys.argv[1] 
 
files = os.listdir(path)
DocumentName = []
for document_name in files:
    if document_name[-5:] == '.docx' and document_name[0] != '~': #open all docx documents:
        DocumentName.append(document_name)
    if document_name[-4:] == '.txt' and document_name[0] != '~': #open all text files
        DocumentName.append(document_name)
    try: #open al pdf files
        file = open(path + document_name, 'rb')
        fileReader = PyPDF2.PdfFileReader(file)
        pageObj = fileReader.getPage(0)
        if len(pageObj.extractText()) >= 5: 
            DocumentName.append(document_name)  
    except:
        continue


# In[27]:


#DocumentName = ['CV Nabil (2).txt'] #only one file

result = pd.DataFrame(DocumentName, columns=['DocumentName'])

result['raw_text'] = result.apply(lambda row: ReadFile(path, row['DocumentName']), axis=1)
result['language'] = result.apply(lambda row: detect_language(row['raw_text']), axis=1)

#Select part of dataset
result = result[10:15]

print(len(result))


# In[28]:


#Bug that needs to be solved
split = result['language']=="Problem with text"
#print(result[split])
result = result[~split]


# In[29]:


#DocumentName_restricted = DocumentName #First x files

startTime = datetime.now()

result['text1'] = result.apply(lambda row: CleanDataTier1(row['raw_text']), axis=1)
result['text1_set'] = transform_toset(result, 'text1', 15)
result['text2'] = result.apply(lambda row: CleanDataTier2(row['text1'], t1_raw_stopwords, row['language'], delete_list=delete_words_nl), axis=1)
result['text3'] = result.apply(lambda row: CleanDataTier3(row['text2'], row['language']), axis=1)
result['text3_set'] = transform_toset(result, 'text3', 15)
result['textpermis'] = result.apply(lambda row: CleanDataTier3(CleanDataTier2(row['text1'], t1_permis_stopwords, row['language'], 0), row['language']), axis=1)
result['text1spaceless'] = result.apply(lambda row: row['text1'].replace(' ',''), axis=1)
#result['text3spaceless'] = result.apply(lambda row: row['text3'].replace(' ',''), axis=1)
result['text3spaceless'] = result['text3'].str.replace(' ', '')


print(datetime.now() - startTime)
result['Jobtitles'] = result.apply(lambda doc_row: jobtitle_search(Jobtitle_restrictive, 'Cleaned1', 'text1', Jobtitle_intermediate, 'Cleaned3', 'text3', 'JobTitle', doc_row), axis=1)

print(datetime.now() - startTime)
result['Companies'] = result.apply(lambda doc_row: list_search1_set(Company_intermediate, 'Cleaned1', 'text1', 'Company', doc_row), axis=1)


print(datetime.now() - startTime)
result['Softwares'] = result.apply(lambda doc_row: list_search1(Softwares_intermediate, 'Cleaned1', 'text1', 'Software', doc_row), axis=1)

print(datetime.now() - startTime)
result['Brevets'] = result.apply(lambda doc_row: list_search1(Brevet_intermediate, 'Cleaned1', 'text1', 'Brevet', doc_row), axis=1)
result['Permis'] = result.apply(lambda doc_row: list_search1(Driverlicense, 'Term', 'textpermis', 'Permis', doc_row), axis=1)


result['email'] = result.apply(lambda doc_row: search_email(doc_row['raw_text']), axis=1) #get email address
result['telephone'] = result.apply(lambda doc_row: search_phone(doc_row['text1spaceless']), axis=1)
result['birthdate'] = result.apply(lambda doc_row: search_birth(doc_row['text1spaceless']), axis=1)


print(datetime.now() - startTime) #print total run time

#result.to_excel('../Data/Output/TestFR3_v1.xlsx',encoding='utf-8', index=False)

#result.reset_index(inplace=True)
result


# In[ ]:


#result = pd.read_excel('../Data/Output/AllDocx6.xlsx') #TestSet
#result = pd.read_excel('../Data/Output/CVConsultants.xlsx') #CVConsultants
#result = pd.read_excel('../Data/Output/NLDocs_spaceless.xlsx')


# # Show outputs

# In[186]:


#i = result[result.DocumentName == 'cv Stepan Hakobyan.1.txt'].index[0] #Find row
i = 0 #Find row

#Print cv name
print(color.BOLD + 'Nom CV: '+ color.END + result.DocumentName[i] + '\n')

#Print jobtitles
print(color.BOLD + 'Fonctions auxquelles il/elle correspond: \n' + color.END)
print(result.Jobtitles[i])


# In[142]:


#Print Companies
print(color.BOLD + 'Entreprises auxquelles il/elle a travaillé: \n' + color.END)
print(result.Companies[i])


# In[114]:


#Print Softwares
print(color.BOLD + 'Programmes informatiques qu\'il/elle maîtrise: \n' + color.END)
print(result.Softwares[i])
print('\n')

#Print Brevets
print(color.BOLD + 'Brevets qu\'il/elle possède: \n' + color.END)
print(result.Brevets[i])
print('\n')

#Print Permis
print(color.BOLD + 'Permis qu\'il/elle possède: \n' + color.END)
print(result.Permis[i])


# In[115]:


#Autres infos:
print(color.BOLD + 'Autres infos: \n' + color.END)

print(color.BOLD + 'Adresse mail: ' + color.END)
print(result.email[i])
print('\n')

print(color.BOLD + 'Numéro de téléphone: ' + color.END)
print(int(result.telephone[i]))
print('\n')

#print(color.BOLD + 'Date de naissance: ' + color.END)
#print(result.birthdate[i])


# In[159]:


#Recherche customisée
print(color.BOLD + 'Résultat recherche: \n' + color.END)
result.text3[i]

