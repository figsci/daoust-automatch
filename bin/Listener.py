from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import threading
import logging
from io import BytesIO
import sys

from Request import *
from Reply import *
from Loader import *
from Reader import *
from Miner import *
from Extractor import *

server = 'server not yet defined'
loader = 'loader not yet defined'
reader = 'reader not yet defined'
miner = 'miner not yet defined'
extractor = 'extractor not yet defined'

n_request_handler = 0

class RequestHandler(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, _server_):
        global n_request_handler
        self.logger = logging.getLogger('Listener.RequestHandler.do_GET')

        if n_request_handler == 0:   # otherwise we have multiple log writing (one per thread)
            formatter = logging.Formatter('%(asctime)s - %(threadName)s - %(name)s - %(levelname)s - %(message)s')
            ch = logging.StreamHandler()
            ch.setLevel(logging.DEBUG)
            ch.setFormatter(formatter)
            self.logger.addHandler(ch)

        n_request_handler += 1

        super().__init__(request, client_address, _server_)

    def do_GET(self):
        global server
        statement = ''
        answer = ''

        self.logger.debug(self.requestline)

        self.send_response(200)
        self.end_headers()

        self.logger.debug('requestline:%s', self.requestline)

        if self.requestline.find('?stop') != -1:
            self.logger.info('STOP statement received...')
            statement = 'STOP'
        elif self.requestline.find('?nthread') != -1:
            self.logger.info('#thread statement received:%d',server.nThread())
            statement = 'N THREAD'
            answer = str(server.nThread())
        else:
            key = '?log='
            start_idx = self.requestline.find(key)
            if start_idx != -1:
                self.logger.debug('LOG statement received...')
                statement = 'LOG LEVEL'
                level = self.requestline[start_idx+len(key):].split(' ')[0]
                self.logger.setLevel(level)

        self.wfile.write(b'get\t' + threading.currentThread().getName().encode() + b'\t' + str(threading.active_count()).encode() + b'\t' + statement.encode() + b'\t' + answer.encode() + b'\n')

        if statement == 'STOP':
            server.shutdown()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        self.logger.info('POST request received')
        self.logger.debug('request_body:%s',body)

        try:
            request = Request(body)
            request.parse()

            cv_list = request.get_cv_list()
            self.logger.debug('#items:%d',len(cv_list))

            candidate_list = []
            url_list = []
            for cv in cv_list:
                candidate_list.append(cv['candidate'])
                url_list.append(cv['cvurl'])

            df_doc = pd.DataFrame()
            df_doc['candidate'] = candidate_list
            df_doc['url'] = url_list

            self.logger.debug('load')
            df_doc = loader.load(df_doc)
            self.logger.debug('read')
            df_doc = reader.read(df_doc)
            self.logger.debug('mine')
            df_doc = miner.mine(df_doc,'content')
            self.logger.debug('extract')
            df_doc = extractor.extract(df_doc)

            self.logger.debug('build reply')
            reply = Reply(request.get_version(), request.get_lang())
            reply_body = reply.build(df_doc)
            self.logger.debug('reply body:%s',reply_body)

            self.send_response(200)
            self.end_headers()

            response = BytesIO()
            response.write(reply_body.encode())
            print(reply_body)

            self.wfile.write(response.getvalue())
            pass
        except Exception as exception:
            logging.error(exception.__str__())
            self.send_response(100)
            self.end_headers()
            response = BytesIO()
            self.wfile.write(exception.__str__().encode())
            pass
        self.logger.info('POST reply sent')
        return


class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


class Listener:
    def __init__(self, port,
                 use_https=False, security_file_dir='.',
                 loader_target_dir='tmp',
                 doc_reader_path='C:/Program Files/LibreOffice/program/soffice.exe',
                 tesseract_path=r'C:\Users\lphilippe\Documents\Agilytic\Daoust\automatch\arc\tesseract-4.0.0-alpha\tesseract.exe',
                 extractor_data_dir='data'):
        global loader, reader, miner, extractor, server

        logger = logging.getLogger('Listener')
        logger.addHandler(logging.StreamHandler())

        self.port = port
        self.use_https = use_https
        self.security_file_dir = security_file_dir
        self.loader_target_dir = loader_target_dir
        self.doc_reader_path = doc_reader_path
        self.tesseract_path = tesseract_path
        self.extractor_data_dir = extractor_data_dir

        self.httpd = ThreadingSimpleServer(('localhost', self.port), RequestHandler)

        if self.use_https:  # not sure it's usable on Windows... to be tried on centos
            import ssl
            self.httpd.socket = ssl.wrap_socket(self.httpd.socket, keyfile=self.security_file_dir+'/'+'key.pem', certfile=self.security_file_dir+'/'+'cert.pem', server_side=True)

        loader = Loader(self.loader_target_dir)
        reader = Reader(doc_reader_path=self.doc_reader_path,
                        tesseract_path=self.tesseract_path)
        miner = Miner()
        extractor = Extractor(self.extractor_data_dir)

        server = self

    def get_port(self):
        return self.port

    def isHTTPS(self):
        return self.use_https

    def nThread(self):
        return threading.active_count()

    def run(self):
        self.httpd.serve_forever(poll_interval=0.5)

    def shutdown(self):
        self.httpd.shutdown()


if __name__ == '__main__':
    server = Listener(1334, False)
    server.run()
