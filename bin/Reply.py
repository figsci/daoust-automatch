import json
import pandas as pd
import numpy as np


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class Reply:
    def __init__(self, version, lang):
        self.version = version
        self.lang = lang

    def build_one_cv(self, candidate, url, job_list):
        one_dict = {'candidate': candidate, 'cvurl':url, 'functions':job_list[:,0]}
        return one_dict

    def build(self, doc_df):
        #
        # check if no row contains error msg
        # If not =>
        doc_df['job_array']= doc_df.apply(lambda row: self.build_one_cv(row['candidate'],row['url'],row['Jobtitles']), axis=1)

        job_array = []
        for job in doc_df['job_array']:
            job_array.append(job)

        reply_dict = {'version': self.version, 'lang': self.lang, 'cv_list': job_array}

        json_text = json.dumps(reply_dict, ensure_ascii=False, cls=NumpyEncoder)

        return json_text


def test_utf8():
    city = "Düsseldorf"
    utf8_encoded = city.encode('utf-8')
    print(type(utf8_encoded))  # bytes
    print(utf8_encoded)  # b'D\xc3\xbcsseldorf'

    decoded_city = utf8_encoded.decode('utf-8')
    print(type(decoded_city))  # str
    print(decoded_city)  # Düsseldorf

    python_array = [decoded_city, 'élastisque', 'école', 'à bientôt', 'sans problème']
    one_dict = {'a': python_array}
    print(json.dumps(one_dict, ensure_ascii=False, cls=NumpyEncoder))
    print(python_array[0])

    print(json.load(json.dumps(one_dict, cls=NumpyEncoder)))

    return


if __name__ == '__main__':
    test_utf8()