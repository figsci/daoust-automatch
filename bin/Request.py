import json


class Request:
    def __init__(self,text):
        self.version = "N.A."
        self.lang = 'fr'
        self.cv_list = {}
        self.text = text
        self.dict = {}

    def parse(self):
        self.dict = json.loads(self.text) # vérification basique de structure JSON correcte
        # vérification de la syntaxe Daoust en fonction de la version de la requête
        self.version = self.dict['version'] # vérif des versions acceptées
        self.lang = self.dict['label_lang'] # vérif des langues acceptées

        self.cv_list = self.dict['cv_list'] # vérif de la syntaxe des tokens: ('candidate'+'url')

        return

    def get_version(self):
        return self.version

    def get_lang(self):
        return self.lang

    def get_cv_list(self):
        return self.cv_list


if __name__ == '__main__':
    request = Request("""{
        "version": "1.1",
		"label_lang": "en",
		"cv_list":[
		{"candidate":"1234545",
		 "url":"server2.daoust.be/CV/?5423"
        }]
        }
""")
    request.parse()
    print('version:{}'.format(request.get_version()))
    print('lang:{}'.format(request.get_lang()))
    print('cv_list:{}'.format(request.get_cv_list()))