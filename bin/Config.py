import configparser
import logging

class Config:
    def __init__(self, config_file_path=""):
        self.config_file_path = config_file_path
        #
        # Vérifier que le fichier de config existe... config.read ne le fait pas mais ne poste pas d'erreur non plus
        #
        self.port = -1
        self.use_https = False
        self.key_file_path = ""
        self.cert_file_path = ""
        self.log_file_path = ""
        self.log_level = logging.INFO

        self.config = configparser.ConfigParser()
        self.config.read(config_file_path)

        self.port = int(self.config['LISTENER']['port'])
        self.log_file_path = self.config['LOGGER']['log_file_path']
        #self.log_level = self.config['LOGGER']['log_level']
        self.log_level = logging.INFO
        self.loader_target_dir = self.config['WORKER']['loader_target_dir']
        self.doc_reader_path = self.config['WORKER']['doc_reader_path']
        self.extractor_data_dir = self.config['WORKER']['extractor_data_dir']
        self.tesseract_path = self.config['WORKER']['tesseract_path']

    def get_port(self):
        return self.port

    def is_https(self):
        return self.use_https

    def get_key_file_path(self):
        return self.key_file_path

    def get_cert_file_path(self):
        return self.cert_file_path

    def get_log_file_path(self):
        return self.log_file_path

    def get_log_level(self):
        return self.log_level

    def get_loader_target_dir(self):
        return self.loader_target_dir

    def get_doc_reader_path(self):
        return self.doc_reader_path

    def get_extractor_data_dir(self):
        return self.extractor_data_dir

    def get_tesseract_path(self):
        return self.tesseract_path

    def get_content(self):
        return self.config

if __name__ == '__main__':
    config = Config()

    content = config.get_content()

    print('config content:{}'.format(content.sections()))
