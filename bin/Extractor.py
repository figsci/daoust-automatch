import pandas as pd
import re

def search_term(term, text):
    # print(term)
    a = re.search(r'\b' + str(term) + ' ', text)
    try:
        return a.start() / len(text)
    except:
        return None


def search_term_all(term, text):
    # print(term)
    a = re.search(str(term), text)
    try:
        return a.start() / len(text)
    except:
        return None


def check_exist(term, text_set):
    return term in text_set


def search_email(text):
    match = re.search(r'[\w\.-]+@[\w\.-]+', text)
    try:
        return match.group(0)
    except:
        return None


def search_driver(text):
    match_list = ['permis b', 'permis de conduire b', 'permis conduire b']
    a = re.search(r'\b' + str(match_list) + ' ', text)
    try:
        a.start()
        return 'Permis B'
    except:
        return None

    # Birth date serach


def search_birth(text):
    a = re.search(r'((0[1-9]|[1-3]\d)(0[1-9]|1[012])[12][09]\d{2})', text)  # regex on different types of birthdates
    try:
        a = a.group()
        return a[:2] + '/' + a[2:4] + '/' + a[4:]
    except:
        return None

    # Phone search


def search_phone(text):
    a = re.search(r'(04\d{8}|4\d{7}|01\d{7}|0032\d{7,9})', text)  # regex on differnt types of phone numbers
    try:
        return a.group()
    except:
        return None


def list_search2_set(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype, doc2_cleaningtype,
                     output_name, doc_row):
    if doc_row['language'] == 'FR':
        search_function = search_term
        term1_df = term1_df[term1_df['Language'] == 'FR']
        term2_df = term2_df[term2_df['Language'] == 'FR']
    if doc_row['language'] == 'NL':
        search_function = search_term_all
        term1_df = term1_df[term1_df['Language'] == 'NL']
        term2_df = term2_df[term2_df['Language'] == 'NL']

    term1_df = term1_df.loc[:, [term1_cleaningtype, output_name]]
    term1_df['exists'] = term1_df.apply(
        lambda term_row: check_exist(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype + '_set']),
        axis=1)  # Get list of all the terms that exist
    term1_df = term1_df[term1_df['exists']]  # Keep only the ones found in set
    try:
        term1_df['position'] = term1_df.apply(
            lambda term_row: search_function(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype]),
            axis=1)  # Get list of all the terms that match
    except:
        pass

    term2_df = term2_df.loc[:, [term2_cleaningtype, output_name]]
    term2_df['exists'] = term2_df.apply(
        lambda term_row: check_exist(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype + '_set']),
        axis=1)  # Get list of all the terms that exist
    term2_df = term2_df[term2_df['exists']]  # Keep only the ones found in set
    try:
        term2_df['position'] = term2_df.apply(
            lambda term_row: search_function(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype]),
            axis=1)  # Get list of all the terms that match
    except:
        pass

    try:
        term_df = term1_df.append(term2_df, sort=True).dropna(subset=['position'])
        term_df = term_df.sort_values(by='position',
                                      ascending=True)  # Sort output by first occurence and only terms that matched
        output = term_df.loc[:, [output_name]].dropna().drop_duplicates(keep='first').values  # Output the unique values
        return output
    except:
        return []


def list_search2(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype, doc2_cleaningtype,
                 output_name, doc_row):
    if doc_row['language'] == 'FR':
        search_function = search_term
        term1_df = term1_df[term1_df['Language'] == 'FR']
        term2_df = term2_df[term2_df['Language'] == 'FR']
    if doc_row['language'] == 'NL':
        search_function = search_term_all
        term1_df = term1_df[term1_df['Language'] == 'NL']
        term2_df = term2_df[term2_df['Language'] == 'NL']

    term1_df = term1_df.loc[:, [term1_cleaningtype, output_name]]
    term2_df = term2_df.loc[:, [term2_cleaningtype, output_name]]
    term1_df['position'] = term1_df.apply(
        lambda term_row: search_function(term_row[term1_cleaningtype], doc_row[doc1_cleaningtype]),
        axis=1)  # Get list of all the terms that match
    term2_df['position'] = term2_df.apply(
        lambda term_row: search_function(term_row[term2_cleaningtype], doc_row[doc2_cleaningtype]),
        axis=1)  # Get list of all the terms that match
    term_df = term1_df.append(term2_df, sort=True).dropna(subset=['position'])
    term_df = term_df.sort_values(by='position',
                                  ascending=True)  # Sort output by first occurrence and only terms that matched
    output = term_df.loc[:, [output_name]].dropna().drop_duplicates(keep='first').values  # Output the unique values
    return output

def jobtitle_search(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype, doc2_cleaningtype,
                    output_name, doc_row):
    if doc_row['language'] == 'FR':
        term2_cleaningtype = 'Cleaned3'
        doc2_cleaningtype = 'text3'
        output = list_search2_set(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,
                                  doc2_cleaningtype, output_name, doc_row)

    if doc_row['language'] == 'NL':
        term2_cleaningtype = 'Cleaned3'
        doc2_cleaningtype = 'text3'
        output = list_search2(term1_df, term1_cleaningtype, doc1_cleaningtype, term2_df, term2_cleaningtype,
                              doc2_cleaningtype, output_name, doc_row)

    return output


class Extractor:
    def __init__(self,input_list_dir):
        self.jobtitle_restrictive = pd.read_csv(input_list_dir + '/jobtitle_restrictive.csv')
        self.jobtitle_intermediate = pd.read_csv(input_list_dir + '/jobtitle_intermediate.csv')

    def extract(self,doc_df):
        doc_df['Jobtitles'] = doc_df.apply(lambda doc_row: jobtitle_search(self.jobtitle_restrictive, 'Cleaned1', 'text1',   # doc_row['text1'] matches terms in jobtitle_restrictive['Cleaned1']
                                                                           self.jobtitle_intermediate, 'Cleaned3', 'text3',
                                                                           'JobTitle', doc_row)
                                           , axis=1)
        return doc_df


def test_extract():
    extractor = Extractor('data')
    doc_df = pd.read_pickle('tmp/doc_df.bin')
    updated_doc_df = extractor.extract(doc_df)
    for job in updated_doc_df['Jobtitles']:
        print(job)
    return


if __name__ == '__main__':
    test_extract()
