import pandas as pd
import langdetect
import unidecode
from nltk.stem.snowball import FrenchStemmer
from nltk.stem.snowball import DutchStemmer
from nltk.corpus import stopwords


def detect_language(text):
    try:
        res = langdetect.detect_langs(text)
        for item in res:
            if item.lang == "fr" or item.lang == "nl":
                return item.lang.upper()
        return None
    except:
        return 'Problem with text'


def cleanDataTier1(data):  # basic cleaning
    if type(data) == str:
        data = unidecode.unidecode(data)
        data = data.replace('(', '').replace(')', '').replace('*', '').replace('+', '').replace('[', '').replace(
            ']', '').replace('|', '').replace('∆', '').replace('?', '')  # remove
        data = data.replace('\t', ' ').replace('\n', ' ').replace(':', ' ').replace('-', ' ').replace(';',
                                                                                                      ' ').replace(
            '\\', ' ').replace('.', ' ').replace(',', ' ').replace('\'', ' ').replace('/', ' ').replace('<',
                                                                                                        ' ')  # replace by spaces
        output = data.lower()  # to lower case
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            element = unidecode.unidecode(element)
            element = element.replace('(', '').replace(')', '').replace('*', '').replace('+', '').replace('[',
                                                                                                          '').replace(
                ']', '').replace('|', '').replace('∆', '').replace('?', '')  # remove
            element = element.replace('\t', ' ').replace('\n', ' ').replace(':', ' ').replace('-', ' ').replace(';',
                                                                                                                ' ').replace(
                '\\', ' ').replace('.', ' ').replace(',', ' ').replace('\'', ' ').replace('/', ' ').replace('<',
                                                                                                            ' ')  # replace by spaces
            element = element.lower()  # to lower case
            output.append(element)
        return output


def cleanDataTier2(data, stopword_df, language, min_len=1, delete_list=[]):  # removing unnecessary words
    stopword_list = stopword_df[stopword_df['Language'] == language]['Stopword_list'].values[0]
    if type(data) == str:
        words = data.split()  # split text into words
        filtered_words = []  # declare an empty list to hold our filtered words
        for word in words:  # iterate over all words from the text
            if word not in stopword_list and len(
                    word) > min_len:  # only add words that are not in the French stopwords list, and are more than 1 character
                filtered_words.append(word)  # add word to filter_words list if it meets the above conditions
        output = ' '.join(filtered_words)

        for d in delete_list:
            output = output.replace(d, '')
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            words = element.split()  # split text into words
            filtered_words = []  # declare an empty list to hold our filtered words
            for word in words:  # iterate over all words from the text
                if word not in stopword_list and len(
                        word) > min_len:  # only add words that are not in the French stopwords list, and are more than 1 character
                    filtered_words.append(word)  # add word to filter_words list if it meets the above conditions
            data = ' '.join(filtered_words)
            for d in delete_list:
                data.replace(d, '')
            output.append(data)
        return output


def cleanDataTier3(data, language):  # stem words
    if language == "FR":
        stemmer = FrenchStemmer()
    if language == "NL":
        stemmer = DutchStemmer()

    if type(data) == str:
        words = data.split()  # split text into words
        stemmed_words = []  # declare an empty list to hold our stemmed words
        for word in words:
            stemmed_word = stemmer.stem(stemmer.stem(stemmer.stem(word)))  # stem the word three times to be sure
            stemmed_words.append(stemmed_word)  # add it to our stemmed word list
        output = ' '.join(stemmed_words)
        return output
    if type(data) == list or pd.core.series.Series:
        output = []
        for element in data:
            words = element.split()  # split text into words
            stemmed_words = []  # declare an empty list to hold our stemmed words
            for word in words:
                stemmed_word = stemmer.stem(
                    stemmer.stem(stemmer.stem(word)))  # stem the word three times to be sure
                stemmed_words.append(stemmed_word)  # add it to our stemmed word list
            data = ' '.join(stemmed_words)
            output.append(data)
        return output


def transform_toset(df, column, combination_length):
    output = []
    splitted = df[column].str.split()
    for index, row in df.iterrows():
        l = len(row.text1)
        for i in range(2, combination_length):
            base = splitted[index]
            inter = []
            for j in range(l - i):
                joined = ' '.join(base[j:j + i])
                inter += [joined]
            base += inter
        output.append(set(base))
    return output


class Miner:
    def __init__(self):
        self.raw_stopword_list_fr = ["Ap.", "Apr.", "GHz", "MHz", "USD", "a", "afin", "ah", "ai", "aie", "aient", "aies",
                                "ait", "alors", "après", "as", "attendu", "au", "au-delà", "au-devant", "aucun",
                                "aucune", "audit", "auprès", "auquel", "aura", "aurai", "auraient", "aurais", "aurait",
                                "auras", "aurez", "auriez", "aurions", "aurons", "auront", "aussi", "autour", "autre",
                                "autres", "autrui", "aux", "auxdites", "auxdits", "auxquelles", "auxquels", "avaient",
                                "avais", "avait", "avant", "avec", "avez", "aviez", "avions", "avons", "ayant", "ayez",
                                "ayons", "b", "bah", "banco", "ben", "bien", "bé", "c", "c'", "c'est", "c'était", "car",
                                "ce", "ceci", "cela", "celle", "celle-ci", "celle-là", "celles", "celles-ci",
                                "celles-là", "celui", "celui-ci", "celui-là", "celà", "cent", "cents", "cependant",
                                "certain", "certaine", "certaines", "certains", "ces", "cet", "cette", "ceux",
                                "ceux-ci", "ceux-là", "cf.", "cg", "cgr", "chacun", "chacune", "chaque", "chez", "ci",
                                "cinq", "cinquante", "cinquante-cinq", "cinquante-deux", "cinquante-et-un",
                                "cinquante-huit", "cinquante-neuf", "cinquante-quatre", "cinquante-sept",
                                "cinquante-six", "cinquante-trois", "cl", "cm", "cm²", "comme", "contre", "d", "d'",
                                "d'après", "d'un", "d'une", "dans", "de", "depuis", "derrière", "des", "desdites",
                                "desdits", "desquelles", "desquels", "deux", "devant", "devers", "dg", "différentes",
                                "différents", "divers", "diverses", "dix", "dix-huit", "dix-neuf", "dix-sept", "dl",
                                "dm", "donc", "dont", "douze", "du", "dudit", "duquel", "durant", "dès", "déjà", "e",
                                "eh", "elle", "elles", "en", "en-dehors", "encore", "enfin", "entre", "envers", "es",
                                "est", "et", "eu", "eue", "eues", "euh", "eurent", "eus", "eusse", "eussent", "eusses",
                                "eussiez", "eussions", "eut", "eux", "eûmes", "eût", "eûtes", "f", "fait", "fi", "flac",
                                "fors", "furent", "fus", "fusse", "fussent", "fusses", "fussiez", "fussions", "fut",
                                "fûmes", "fût", "fûtes", "g", "gr", "h", "ha", "han", "hein", "hem", "heu", "hg", "hl",
                                "hm", "hm³", "holà", "hop", "hormis", "hors", "huit", "hum", "hé", "i", "ici", "il",
                                "ils", "j", "j'", "j'ai", "j'avais", "j'étais", "jamais", "je", "jusqu'", "jusqu'au",
                                "jusqu'aux", "jusqu'à", "jusque", "k", "kg", "km", "km²", "l", "l'", "l'autre", "l'on",
                                "l'un", "l'une", "la", "laquelle", "le", "lequel", "les", "lesquelles", "lesquels",
                                "leur", "leurs", "lez", "lors", "lorsqu'", "lorsque", "lui", "lès", "m", "m'", "ma",
                                "maint", "mainte", "maintes", "maints", "mais", "malgré", "me", "mes", "mg", "mgr",
                                "mil", "mille", "milliards", "millions", "ml", "mm", "mm²", "moi", "moins", "mon",
                                "moyennant", "mt", "m²", "m³", "même", "mêmes", "n", "n'avait", "n'y", "ne", "neuf",
                                "ni", "non", "nonante", "nonobstant", "nos", "notre", "nous", "nul", "nulle", "nº",
                                "néanmoins", "o", "octante", "oh", "on", "ont", "onze", "or", "ou", "outre", "où", "p",
                                "par", "par-delà", "parbleu", "parce", "parmi", "pas", "passé", "pendant", "personne",
                                "peu", "plus", "plus_d'un", "plus_d'une", "plusieurs", "pour", "pourquoi", "pourtant",
                                "pourvu", "près", "puisqu'", "puisque", "q", "qu", "qu'", "qu'elle", "qu'elles",
                                "qu'il", "qu'ils", "qu'on", "quand", "quant", "quarante", "quarante-cinq",
                                "quarante-deux", "quarante-et-un", "quarante-huit", "quarante-neuf", "quarante-quatre",
                                "quarante-sept", "quarante-six", "quarante-trois", "quatorze", "quatre", "quatre-vingt",
                                "quatre-vingt-cinq", "quatre-vingt-deux", "quatre-vingt-dix", "quatre-vingt-dix-huit",
                                "quatre-vingt-dix-neuf", "quatre-vingt-dix-sept", "quatre-vingt-douze",
                                "quatre-vingt-huit", "quatre-vingt-neuf", "quatre-vingt-onze", "quatre-vingt-quatorze",
                                "quatre-vingt-quatre", "quatre-vingt-quinze", "quatre-vingt-seize", "quatre-vingt-sept",
                                "quatre-vingt-six", "quatre-vingt-treize", "quatre-vingt-trois", "quatre-vingt-un",
                                "quatre-vingt-une", "quatre-vingts", "que", "quel", "quelle", "quelles", "quelqu'",
                                "quelqu'un", "quelqu'une", "quelque", "quelques", "quelques-unes", "quelques-uns",
                                "quels", "qui", "quiconque", "quinze", "quoi", "quoiqu'", "quoique", "r", "revoici",
                                "revoilà", "rien", "s", "s'", "sa", "sans", "sauf", "se", "seize", "selon", "sept",
                                "septante", "sera", "serai", "seraient", "serais", "serait", "seras", "serez", "seriez",
                                "serions", "serons", "seront", "ses", "si", "sinon", "six", "soi", "soient", "sois",
                                "soit", "soixante", "soixante-cinq", "soixante-deux", "soixante-dix",
                                "soixante-dix-huit", "soixante-dix-neuf", "soixante-dix-sept", "soixante-douze",
                                "soixante-et-onze", "soixante-et-un", "soixante-et-une", "soixante-huit",
                                "soixante-neuf", "soixante-quatorze", "soixante-quatre", "soixante-quinze",
                                "soixante-seize", "soixante-sept", "soixante-six", "soixante-treize", "soixante-trois",
                                "sommes", "son", "sont", "sous", "soyez", "soyons", "suis", "suite", "sur", "sus", "t",
                                "t'", "ta", "tacatac", "tandis", "te", "tel", "telle", "telles", "tels", "tes", "toi",
                                "ton", "toujours", "tous", "tout", "toute", "toutefois", "toutes", "treize", "trente",
                                "trente-cinq", "trente-deux", "trente-et-un", "trente-huit", "trente-neuf",
                                "trente-quatre", "trente-sept", "trente-six", "trente-trois", "trois", "très", "tu",
                                "u", "un", "une", "unes", "uns", "v", "vers", "via", "vingt", "vingt-cinq",
                                "vingt-deux", "vingt-huit", "vingt-neuf", "vingt-quatre", "vingt-sept", "vingt-six",
                                "vingt-trois", "vis-à-vis", "voici", "voilà", "vos", "votre", "vous", "w", "x", "y",
                                "z", "zéro", "à", "ç'", "ça", "ès", "étaient", "étais", "était", "étant", "étiez",
                                "étions", "été", "étée", "étées", "étés", "êtes", "être", "ô"]
        self.raw_stopword_list_nl = ['a', 'aan', 'aangaande', 'aangezien', 'achter', 'achterna', 'aen', 'af', 'afd',
                                'afgelopen', 'agter', 'al', 'aldaar', 'aldus', 'alhoewel', 'alias', 'alle', 'allebei',
                                'alleen', 'alleenlyk', 'allen', 'alles', 'als', 'alsnog', 'altijd', 'altoos', 'altyd',
                                'ander', 'andere', 'anderen', 'anders', 'anderszins', 'anm', 'b', 'behalve',
                                'behoudens', 'beide', 'beiden', 'ben', 'beneden', 'bent', 'bepaald', 'beter', 'betere',
                                'betreffende', 'bij', 'bijna', 'bijvoorbeeld', 'bijv', 'binnen', 'binnenin', 'bizonder',
                                'bizondere', 'bl', 'blz', 'boven', 'bovenal', 'bovendien', 'bovengenoemd',
                                'bovenstaand', 'bovenvermeld', 'buiten', 'by', 'daar', 'daarheen', 'daarin', 'daarna',
                                'daarnet', 'daarom', 'daarop', 'daarvanlangs', 'daer', 'dan', 'dat', 'de', 'deeze',
                                'den', 'der', 'ders', 'derzelver', 'des', 'deszelfs', 'deszelvs', 'deze', 'dezelfde',
                                'dezelve', 'dezelven', 'dezen', 'dezer', 'dezulke', 'die', 'dien', 'dikwijls',
                                'dikwyls', 'dit', 'dl', 'doch', 'doen', 'doet', 'dog', 'door', 'doorgaand', 'doorgaans',
                                'dr', 'dra', 'ds', 'dus', 'echter', 'ed', 'een', 'eene', 'eenen', 'eener', 'eenig',
                                'eenige', 'eens', 'eer', 'eerdat', 'eerder', 'eerlang', 'eerst', 'eerste', 'eersten',
                                'effe', 'egter', 'eigen', 'eigene', 'elk', 'elkanderen', 'elkanderens', 'elke', 'en',
                                'enig', 'enige', 'enigerlei', 'enigszins', 'enkel', 'enkele', 'enz', 'er', 'erdoor',
                                'et', 'etc', 'even', 'eveneens', 'evenwel', 'ff', 'gauw', 'ge', 'gebragt', 'gedurende',
                                'geen', 'geene', 'geenen', 'gegeven', 'gehad', 'geheel', 'geheele', 'gekund', 'geleden',
                                'gelijk', 'gelyk', 'gemoeten', 'gemogen', 'geven', 'geweest', 'gewoon', 'gewoonweg',
                                'geworden', 'gezegt', 'gij', 'gt', 'gy', 'haar', 'had', 'hadden', 'hadt', 'haer',
                                'haere', 'haeren', 'haerer', 'hans', 'hare', 'heb', 'hebben', 'hebt', 'heeft', 'hele',
                                'hem', 'hen', 'het', 'hier', 'hierbeneden', 'hierboven', 'hierin', 'hij', 'hoe',
                                'hoewel', 'hun', 'hunne', 'hunner', 'hy', 'ibid', 'idd', 'ieder', 'iemand', 'iet',
                                'iets', 'ii', 'iig', 'ik', 'ikke', 'ikzelf', 'in', 'indien', 'inmiddels', 'inz',
                                'inzake', 'is', 'ja', 'je', 'jezelf', 'jij', 'jijzelf', 'jou', 'jouw', 'jouwe', 'juist',
                                'jullie', 'kan', 'klaar', 'kon', 'konden', 'krachtens', 'kunnen', 'kunt', 'laetste',
                                'lang', 'later', 'liet', 'liever', 'like', 'm', 'maar', 'maeken', 'maer', 'mag',
                                'martin', 'me', 'mede', 'meer', 'meesten', 'men', 'menigwerf', 'met', 'mezelf', 'mij',
                                'mijn', 'mijnent', 'mijner', 'mijzelf', 'min', 'minder', 'misschien', 'mocht',
                                'mochten', 'moest', 'moesten', 'moet', 'moeten', 'mogelijk', 'mogelyk', 'mogen', 'my',
                                'myn', 'myne', 'mynen', 'myner', 'myzelf', 'na', 'naar', 'nabij', 'nadat', 'naer',
                                'net', 'niet', 'niets', 'nimmer', 'nit', 'no', 'noch', 'nog', 'nogal', 'nooit', 'nr',
                                'nu', 'o', 'of', 'ofschoon', 'om', 'omdat', 'omhoog', 'omlaag', 'omstreeks', 'omtrent',
                                'omver', 'onder', 'ondertussen', 'ongeveer', 'ons', 'onszelf', 'onze', 'onzen', 'onzer',
                                'ooit', 'ook', 'oorspr', 'op', 'opdat', 'opnieuw', 'opzij', 'opzy', 'over', 'overeind',
                                'overigens', 'p', 'pas', 'pp', 'precies', 'pres', 'prof', 'publ', 'reeds', 'rond',
                                'rondom', 'rug', 's', 'sedert', 'sinds', 'sindsdien', 'sl', 'slechts', 'sommige',
                                'spoedig', 'st', 'steeds', 'sy', 't', 'tamelijk', 'tamelyk', 'te', 'tegen', 'tegens',
                                'ten', 'tenzij', 'ter', 'terwijl', 'terwyl', 'thans', 'tijdens', 'toch', 'toe', 'toen',
                                'toenmaals', 'toenmalig', 'tot', 'totdat', 'tusschen', 'tussen', 'tydens', 'u', 'uit',
                                'uitg', 'uitgezonderd', 'uw', 'uwe', 'uwen', 'uwer', 'vaak', 'vaakwat', 'vakgr', 'van',
                                'vanaf', 'vandaan', 'vanuit', 'vanwege', 'veel', 'veeleer', 'veelen', 'verder', 'verre',
                                'vert', 'vervolgens', 'vgl', 'vol', 'volgens', 'voor', 'vooraf', 'vooral', 'vooralsnog',
                                'voorbij', 'voorby', 'voordat', 'voordezen', 'voordien', 'voorheen', 'voorop', 'voort',
                                'voortgez', 'voorts', 'voortz', 'vooruit', 'vrij', 'vroeg', 'vry', 'waar', 'waarom',
                                'wanneer', 'want', 'waren', 'was', 'wat', 'we', 'weer', 'weg', 'wege', 'wegens',
                                'weinig', 'weinige', 'wel', 'weldra', 'welk', 'welke', 'welken', 'welker', 'werd',
                                'werden', 'werdt', 'wezen', 'wie', 'wiens', 'wier', 'wierd', 'wierden', 'wij',
                                'wijzelf', 'wil', 'wilde', 'worden', 'wordt', 'wy', 'wyze', 'wyzelf', 'zal', 'ze',
                                'zeer', 'zei', 'zeker', 'zekere', 'zelf', 'zelfde', 'zelfs', 'zelve', 'zelven', 'zelvs',
                                'zich', 'zichzelf', 'zichzelve', 'zichzelven', 'zie', 'zig', 'zij', 'zijn', 'zijnde',
                                'zijne', 'zijner', 'zo', "zo'n", 'zoals', 'zodra', 'zommige', 'zommigen', 'zonder',
                                'zoo', 'zou', 'zoude', 'zouden', 'zoveel', 'zowat', 'zulk', 'zulke', 'zulks', 'zullen',
                                'zult', 'zy', 'zyn', 'zynde', 'zyne', 'zynen', 'zyner', 'zyns']
        self.delete_words_nl = ['medewerker', 'medewerkster', 'arbeider', 'arbeidster', 'werker', 'werkster', 'bediende']
        self.t1_raw_stopword_dict = {
            'Stopword_list': [cleanDataTier1(self.raw_stopword_list_fr), cleanDataTier1(self.raw_stopword_list_nl)],
            'Language': ['FR', 'NL']}
        self.t1_raw_stopwords = pd.DataFrame(self.t1_raw_stopword_dict)

        self.permis_stopword_list_fr = [word for word in self.raw_stopword_list_fr if
                                   word not in ["b", "be", "c", "ce", "e", "d", "g", "am"]]
        self.permis_stopword_list_nl = [word for word in self.raw_stopword_list_nl if
                                   word not in ["b", "be", "c", "ce", "e", "d", "g", "am"]]
        self.t1_permis_stopword_dict = {
            'Stopword_list': [cleanDataTier1(self.permis_stopword_list_fr), cleanDataTier1(self.permis_stopword_list_nl)],
            'Language': ['FR', 'NL']}
        self.t1_permis_stopwords = pd.DataFrame(self.t1_permis_stopword_dict)

        return

    def mine(self,doc_df,content_column_name='raw_text'):
        doc_df['language'] = doc_df.apply(lambda row: detect_language(row[content_column_name]), axis=1)
        doc_df['text1'] = doc_df.apply(lambda row: cleanDataTier1(row[content_column_name]), axis=1)
        doc_df['text1_set'] = transform_toset(doc_df, 'text1', 15)
        doc_df['text2'] = doc_df.apply(
            lambda row: cleanDataTier2(row['text1'], self.t1_raw_stopwords, row['language'], delete_list=self.delete_words_nl),
            axis=1)
        doc_df['text3'] = doc_df.apply(lambda row: cleanDataTier3(row['text2'], row['language']), axis=1)
        doc_df['text3_set'] = transform_toset(doc_df, 'text3', 15)

        return doc_df


def test_mine():
    miner = Miner()
    doc_df = pd.DataFrame(["""17/01/2018 1/2 
Ayumi MIZUTANI 
Tel. : +32 484 10 22 28 Rue d’Anderlecht 24 
E-mail : l.a.mizutani@gmail.com 1000 Bruxelles 
EXPERIENCE PROFESSIONNELLE 
2012 - 2017 : THALYS INTERNATIONAL / Bruxelles, Belgique 
2014 – 2017 | Ingénieur domaine Confort, Aménagement Intérieur et Nettoyage pour la Direction du Matériel 
Référente Confort du parc de trains Thalys (26 rames) 
o Pilotage du groupe de travail confort (partenaires SNCF, SNCB) 
o Suivi des indicateurs de satisfaction client 
o Mise en place d’actions issues du retour d’expérience 
o Veille auprès des ateliers de maintenance et ingénierie SNCF 
Gestion des évolutions techniques ou de règles de maintenance 
o Pilotage des modifications : études (analyse des besoins et des risques, coûts, délais), commandes auprès de 
l’ingénierie SNCF/SNCB, planification et suivi du déploiement, communication auprès des ateliers et partenaires, 
modification des documents de maintenance 
o Coordination des tests sur rame avec les fournisseurs pour les évolutions techniques (pelliculage, diffuseur de 
parfum à bord, …) 
Déploiement des opérations de contrôle « Vision Client » dans les ateliers de maintenance 
o Observations terrain hebdomadaires contribuant à l’amélioration continue 
o Projet de « Remise à niveau confort » - logistique, gestion des approvisionnements, pilotage de la mise à 
jour/création des plans (pièces confort) 
2012 – 2014 | Customer Information Systems Officer pour la Production Opérationnelle 
Project Manager des projets IT pour le centre opérationnel Thalys 
o Expression fonctionnelle des besoins métier 
o Pilotage et négociations auprès des prestataires externes chargés des développements logiciels : gestion 
contractuelle, demande de chiffrages, suivi, reporting 
o Planification des développements et suivi d’un budget de 300 k€ / an 
o Phase de tests : contrôle et validation des développements avant mise en production 
o Conduite du changement auprès des utilisateurs, documentations, formations 
o Collaboration avec les prestataires externes et les équipes internes E-commerce/ IT 
o Rôle d’administrateur auprès des utilisateurs 
Ingénieur secteur ferroviaire / Project management 
Diplôme d’ingénieur généraliste 
5 ans d’expérience chez Thalys (Belgique) : en Production opérationnelle (gestion de projet IT) et à la Direction 
du matériel (amélioration continue) 
Formation en management de l’entreprise et en gestion de projets innovants 
Objectif professionnel 
1/ Accompagnement stratégique au sein d’une start-up 
2/ Auditeur au sein d’une PME ou en tant que consultante dans un secteur industriel 
3/ Manager d’une équipe de 4-5 personnes 

17/01/2018 Ayumi MIZUTANI / Tel. : +32 484 10 22 28 2/2 
2011 - 2012 : GROUPE SEB / Selongey, France 
2011- 2012 | Assistante chef de projet recherche innovation pour le pôle Electrique culinaire 
Gestion d’un projet de conception d’une solution innovante de cuiseur vapeur 
o Planification, réalisation de prototypes, suivi des jalons 
o Coordination des acteurs du projet: marketing, bureau d’études, partenaires extérieurs 
o Benchmarking, rédaction de mémoires techniques en vue de la rédaction de brevets, réalisation d’essais 
FORMATIONS 
2017 | Formation de 4 mois au management pratique des entreprises – Bruxelles-Formation, Optimum 
management 
Approche managériale : stratégie, analyse financière, marketing, études de marché, ressources humaines, leadership 
2016 | Formation au Lean management (Yellow Belt) – Avertim 
2011 | Diplôme d’ingénieur généraliste, ENSGSI (Ecole Nationale Supérieure en Génie des Systèmes et de 
l’Innovation, Université de Lorraine, France) 
Gestion de projet, Conception/ innovation (de l’idée à la commercialisation), management et développement personnel, 
ingénierie système, génie industriel 
2006 | Baccalauréat international scientifique, spécialité mathématiques (Grenoble, France) 
LANGUES 
Français : langue maternelle 
Anglais (niveau B1), réunions professionnelles en anglais dans le cadre d’un projet Railteam en 2014 
Italien (avancé), 12 ans de cours d’italien (baccalauréat international italien à Grenoble) 
Japonais (niveau A2), cours du soir entre 2012 et 2014 et voyages réguliers au Japon 
Néerlandais (niveau A1), cours du soir en 2017 (La Maison du Néerlandais) 
INFORMATIQUE 
Pack Office, MS Project, Navision 
Bonne connaissance et utilisation des logiciels internes développés spécifiquement pour Thalys. 
CENTRES D’INTERET 
Pratique régulière de diverses danses swing : Lindy Hop, Charleston, Blues 
Escalade, ski, course à pied 
Voyages : Japon, Thaïlande, Europe 
DONNEES PERSONNELLES 
Née le 03/11/1988 (29 ans) 
Nationalité Française 
Permis B 
Disponible immédiatement 

"""],columns=['raw_text'])
    updated_doc_df = miner.mine(doc_df)
    print(updated_doc_df)
    updated_doc_df.to_csv('tmp/doc_df.csv',sep=';')
    updated_doc_df.to_pickle('tmp/doc_df.bin')

if __name__ == '__main__':
    test_mine()               ### fonctionne du premier coup... assez bluffant :)
