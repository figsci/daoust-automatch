import argparse
import logging

from Config import *
from Listener import *

parser = argparse.ArgumentParser(description='AutoMatch @ Agilytic 2019')

parser.add_argument('--version', '-v',  action='version', version='%(prog)s 1.2')
parser.add_argument('--config', '-c', default='../cfg/automatch.ini', help='set configuration file path (relative to CWD)')

results = parser.parse_args()

print(results)

config = Config(results.config)
logging.basicConfig(filename=config.get_log_file_path(),
                    level=config.get_log_level(),
                    format='%(asctime)s-%(levelname)s-%(threadName)s-%(module)s-%(funcName)s:%(message)s')

logging.info('port:{}'.format(config.get_port()))

listener = Listener(port=config.get_port(),
                    use_https=config.is_https(),
                    loader_target_dir=config.get_loader_target_dir(),
                    doc_reader_path=config.get_doc_reader_path(),
                    extractor_data_dir=config.get_extractor_data_dir(),
                    tesseract_path=config.get_tesseract_path())

listener.run()

logging.info('end')
