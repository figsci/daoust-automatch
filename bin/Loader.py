import os
import pandas as pd
#
# It's not a real loader (from daoust cv server) but it's returning file path in temp directory based
# on file_type key in ('std_pdf','complex_pdf','pdf_with_image','doc','docx','png')
#
class Loader:
    def __init__(self,temp_directory):
        # check if temp_directory does exist
        temp_directory_filepath = os.getcwd()+"/"+temp_directory
        if os.path.isdir(temp_directory):
            self.temp_directory = temp_directory
        else:
            raise OSError('temp_directory:{} does not exist!'.format(temp_directory))

        self.data_directory = "data"
        if os.path.isdir(self.data_directory) == False:
            raise OSError('data_directory:{} does not exist!'.format(self.data_directory))

        self.test_data_file_map = {'doc':'CV_SALES_MANAGER_2017_OK.doc',                     # "pseudo-url":"file_name" present in data subdirectory
                    'docx':'GustinTyfanieCvVendeuse.docx',
                    'png':'piqueray_michel_cv_op.png',
                    'std_pdf':'cv_laurent.pdf',
                    'txt':'2018_01_17_CV_Ayumi_Mizutani.txt',
                    'pdf_with_image':'Bashouri_Luay_cv.pdf',
                    'complex_pdf':'2018_01_17_CV_Ayumi_Mizutani.pdf'}

        for file in self.test_data_file_map.values():
            test_data_file_path = self.data_directory+"/"+file
            if os.path.isfile(test_data_file_path) == False:
                print('file:{}'.format(file))
                raise OSError('test data file:{} does not exist!'.format(test_data_file_path))

    def uni_load(self,url):
        if url in self.test_data_file_map.keys():
            # copy file from data to tmp
            source_file_path = self.data_directory+"/"+self.test_data_file_map[url]
            dest_file_path = self.temp_directory+"/"+self.test_data_file_map[url]
            open(dest_file_path, 'wb').write(open(source_file_path,'rb').read())
            return dest_file_path
        else:
            raise OSError('file type:\'{}\' not found in \'{}\''.format(url,self.test_data_file_map.keys()))

    def load(self,url_df):
        file_path_list = []
        error_msg_list = []

        for url in url_df['url']:
            file_path = ""
            error_msg = ""
            try:
                file_path = self.uni_load(url)
                pass
            except Exception as exception:
                error_msg = exception.__str__()
            file_path_list.append(file_path)
            error_msg_list.append(error_msg)

        url_df['file_path'] = file_path_list
        url_df['load_error'] = error_msg_list

        return url_df


def test_load():
    loader = Loader("tmp")
    file_type = "std_pdf"
    loaded_file_path = loader.uni_load(file_type)
    print('file type:{} => loaded_file_path:{}'.format(file_type, loaded_file_path))


def test_multi_load():
    df = pd.DataFrame(['std_pdf','txt'],columns=['url'])

    loader = Loader('tmp')
    completed_df = loader.load(df)


if __name__ == '__main__':
    test_multi_load()