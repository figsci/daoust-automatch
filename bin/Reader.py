import pandas as pd
import os
import pathlib
import subprocess

import unidecode
import PyPDF2
import wand.image
import cv2
import pytesseract
from PIL import Image


class Reader:

    def __init__(self, temporary_path='tmp', doc_reader_path='', tesseract_path=''):
        self.switcher = {'.txt':self.read_text,
                         '.doc':self.read_doc,
                         '.docx':self.read_doc,
                         '.pdf':self.read_pdf,
                         '.png':self.read_image
                         }

        self.temporary_path = temporary_path
        self.doc_reader_path = doc_reader_path
        self.tesseract_bin_path = tesseract_path

        return

    def preprocess_image(self, fullpath):
        directory = fullpath.rsplit('/', 1)[0]
        image = cv2.imread(fullpath)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        filename = "{}/{}.png".format(directory, os.getpid())
        cv2.imwrite(filename, gray)
        return filename

    def pdf_to_image_to_text(self, fullpath):
        # Take apart PDF into individual image for each page
        directory = fullpath.rsplit('/', 1)[0]
        text = ''
        with wand.image.Image(filename=fullpath, resolution=500) as pdf:
            for i, page in enumerate(pdf.sequence):
                with wand.image.Image(page) as img:
                    filename = "{}/{}_{}.png".format(directory, os.getpid(), i + 1)
                    img.format = 'png'
                    img.alpha_channel = False  # Set to false to keep white background
                    img.save(filename=filename)
                    preprocessed_image = self.preprocess_image(filename)
                    os.remove(filename)
                    text += self.read_image(preprocessed_image)
        return text

    def read_text(self, file_path):
        try:
            return unidecode.unidecode(open(file_path, encoding='utf-8').read())
        except UnicodeDecodeError:
            return unidecode.unidecode(open(file_path, encoding='latin-1').read())
        except:
            raise OSError('Not able to read text file:{} in [utf-8,latin-1] coding'.format(file_path))

    def read_doc(self, file_path):
        content = ''
        try:
            subprocess.call([self.doc_reader_path, '--headless', '--convert-to', 'txt', file_path, '--outdir', self.temporary_path])
            text_file_path = self.temporary_path+'/'+pathlib.PurePosixPath(file_path).stem+'.txt'
            content = self.read_text(text_file_path)
            os.remove(text_file_path)
        except Exception as exception:
            raise OSError('Not able to read doc file:{} because:{}'.format(file_path,exception.__str__()))
        return content

    def read_pdf(self, file_path):
        text = ''
        input_file = open(file_path, 'rb')
        file_reader = PyPDF2.PdfFileReader(input_file)
        for p in range(file_reader.numPages):
            page = file_reader.getPage(p)
            text += page.extractText()

        clean_text = text.replace('\n', '')
        if len(clean_text) >= 5:
            return unidecode.unidecode(text)
        else:
            text = self.pdf_to_image_to_text(file_path)
            return text
            #raise OSError('Not able to read pdf file:{}. It s probably a scanned file...'.format(file_path))

    def read_image(self, fullpath):
        #pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
        text = pytesseract.image_to_string(Image.open(fullpath),)
        #os.remove(fullpath)
        return text

    def uni_read(self,file_path):
        if os.path.isfile(file_path) == False:
            raise OSError('file_path:\'{}\' is not a real file!'.format(file_path))

        extension = pathlib.PurePosixPath(file_path).suffix
        if extension in self.switcher.keys():
            read_function = self.switcher.get(extension)
            return read_function(file_path)
        else:
            raise OSError('file extension:\'{}\' not found in \'{}\''.format(extension, self.switcher.keys()))

    def read(self,file_df,file_path_column='file_path'):
        file_path_list = file_df[file_path_column]
        error_msg_list = []
        content_list = []
        for file_path in file_path_list:
            content = ''
            error_msg = ''
            try:
                content = self.uni_read(file_path)
                pass
            except Exception as exception:
                error_msg = exception.__str__()
            content_list.append(content)
            error_msg_list.append(error_msg)

        file_df['content'] = content_list
        file_df['read_error'] = error_msg_list
        return file_df

    def read_image(self, fullpath):
        pytesseract.pytesseract.tesseract_cmd = self.tesseract_bin_path
        text = pytesseract.image_to_string(Image.open(fullpath), )
        #os.remove(fullpath) by the caller!!!
        return text


test_file_path_list = ['data/text_ucs-2_be_bom.txt','data/text_utf8.txt','data/text_ansi.txt','data/text_bin.txt']


def test_uni_read():
    global test_file_path_list
    for file_path in test_file_path_list:
        try:
            reader = Reader()
            content = reader.uni_read(file_path)
            print('content:{}'.format(content))
        except Exception as exception:
            print(exception.__str__())


def test_read():
    global test_file_path_list
    test_file_path_list.append('data/CV_SALES_MANAGER_2017_OK.doc')
    test_file_path_list.append('data/cv_laurent.pdf')
    test_file_path_list = []
    test_file_path_list.append('data/2018_01_17_CV_Ayumi_Mizutani.pdf')

    file_def_column_name = 'file_path'
    file_df = pd.DataFrame(test_file_path_list,columns=[file_def_column_name])

    try:
        reader = Reader(doc_reader_path='C:/Program Files/LibreOffice/program/soffice.exe',
                        tesseract_path=r'C:\Users\lphilippe\Documents\Agilytic\Daoust\automatch\arc\tesseract-4.0.0-alpha\tesseract.exe')
        file_df = reader.read(file_df,file_def_column_name)
    except Exception as exception:
        print(exception.__str__())

    print(file_df)


if __name__ == '__main__':
    test_read()